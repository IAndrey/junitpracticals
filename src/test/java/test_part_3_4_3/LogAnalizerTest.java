package test_part_3_4_3;

import org.junit.Assert;
import org.junit.Test;
import part_3_4_3.LogAnalyzer;

public class LogAnalizerTest {

    @Test
    public void isValidFileName_BadExtension_ReturnsTrue(){
        FakeExtensionManager fakeExtensionManager = new FakeExtensionManager();
        fakeExtensionManager.willBeValid = true;
        LogAnalyzer logAnalyzer = new LogAnalyzer(fakeExtensionManager);
        Assert.assertTrue(logAnalyzer.isValidLogFileName(""));
    }
}
