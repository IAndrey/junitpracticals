package test_part_3_4_6;

import org.junit.Assert;
import org.junit.Test;
import part_3_4_6.ExtensionManagerFactory;
import part_3_4_6.LogAnalyzer;
import test_part_3_4_3.FakeExtensionManager;

/**
 * Проверка имени файла в LogAnalyzer происходит с помощью FileExtensionManager
 * ExtensionManagerFactory - фабрика для создания объектов FileExtensionManager.
 * Для работы заглушки исп интерфейс IExtensionManager, заглушка и FileExtensionManager
 * наследуются от него. Заглушка просто возвращает нужную bool переменную. 
 */
public class LogAnalizerTest {

    @Test
    public void isValidFileName_BadExtension_ReturnsTrue(){
        FakeExtensionManager fakeExtensionManager = new FakeExtensionManager();
        fakeExtensionManager.willBeValid = true;

        ExtensionManagerFactory.setManager(fakeExtensionManager);

        LogAnalyzer logAnalyzer = new LogAnalyzer();
        Assert.assertTrue(logAnalyzer.isValidLogFileName(""));
    }
}
