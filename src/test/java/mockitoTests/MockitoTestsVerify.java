package mockitoTests;

import mockito.DataSearchRequest;
import mockito.DataService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

public class MockitoTestsVerify {

    @Mock
    DataService dataService;

    @Before
    public void beforeMethod() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void callMethod_getDataById() {
        dataService.getDataById("1");
        Mockito.verify(dataService).getDataById(any());
    }

    @Test
    public void callMethodTimes_getDataById() {
        dataService.getDataById("1");
        dataService.getDataById("2");
        dataService.getDataById("2");
        dataService.getDataById("3");
        Mockito.verify(dataService, Mockito.times(1)).getDataById("1");
        Mockito.verify(dataService, Mockito.times(2)).getDataById("2");
        Mockito.verify(dataService, Mockito.times(1)).getDataById("3");
        Mockito.verify(dataService, Mockito.times(4)).getDataById(any());
        Mockito.verify(dataService, Mockito.never()).getDataById("4");

        dataService.getDataById("4");
        Mockito.verify(dataService, Mockito.times(1)).getDataById("4");

//        dataService.getDataById("5"); verifyNoMoreInteractions - выдаст ошибку если раскоментировать
//        вызов этого метода, т.к. на вызов метода getDataById("5") не навешен ни один verify
        Mockito.verifyNoMoreInteractions(dataService);
    }

    @Test
    public void callMethodTimesWithADelay_getDataById() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        dataService.getDataById("1");
        dataService.getDataById("2");
        dataService.getDataById("3");
        Mockito.verify(dataService, Mockito.after(4000).times(3)).getDataById(any());
    }

    @Test
    public void callMethodInOrder_getDataById() {
        List<String> dataToSave = new ArrayList<>();
        dataToSave.add("1");
        dataToSave.add("2");
        InOrder inOrder = Mockito.inOrder(dataService);
        dataService.saveData(dataToSave);
        dataService.saveData(dataToSave);
        dataService.getData();
//      Проверка должна быть только в таком порядке, т.к. проверяется что сначала был вызван метод saveData(...)
//      два раза, а после уже был вызван метод getData(). Если сначала указать
//      inOrder.verify(dataService).getData();
//      inOrder.verify(dataService, Mockito.times(2)).saveData(any());
//      То тест будет не пройдет, т.к методы были вызваны в другой последовательности.
        inOrder.verify(dataService, Mockito.times(2)).saveData(any());
        inOrder.verify(dataService).getData();
    }

    //Если параметр метода - объект со сложной структурой и нам нужно проверить св-ва объекта, переданного в метод.
    //ВАЖНО:
    //Не очень удобно, что перехват параметра обязательно комбинируется с контролем количества вызовов, но это мелочь.
    @Test
    public void capture_DataSearchRequest() {
        DataSearchRequest request = new DataSearchRequest("idValue", new Date(System.currentTimeMillis()), 50);
        dataService.getDataByRequest(request);
        ArgumentCaptor<DataSearchRequest> requestCaptor = ArgumentCaptor.forClass(DataSearchRequest.class);
        Mockito.verify(dataService, Mockito.times(1)).getDataByRequest(requestCaptor.capture());
        Assert.assertEquals(1, requestCaptor.getAllValues().size());
        DataSearchRequest capturedArgument = requestCaptor.getValue();
        Assert.assertEquals("idValue", capturedArgument.getId());
        Assert.assertTrue(capturedArgument.getUpdatedBefore().before(new Date(1980, Calendar.MARCH, 3)));
        Assert.assertTrue(capturedArgument.getLength() > 1);
    }
}
