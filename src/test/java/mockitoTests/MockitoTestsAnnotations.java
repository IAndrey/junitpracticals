package mockitoTests;

import mockito.DataService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;

public class MockitoTestsAnnotations {

    @Mock
    DataService dataService;

    @Before
    public void beforeMethod() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void callMethod_getDataById() {
        dataService.getDataById("1");
        Mockito.verify(dataService).getDataById(any());
    }
}
