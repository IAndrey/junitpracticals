package mockitoTests;

import mockito.DataService;
import mockito.MyOverridableClass;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;


public class MockitoTestsWhen {

    @Test
    public void getData() {
        DataService dataService = Mockito.mock(DataService.class);
        List<String> data = new ArrayList<>();
        data.add("dataItem");
        Mockito.when(dataService.getData()).thenReturn(data);
        Assert.assertEquals(data, dataService.getData());
    }

    @Test
    public void getDataById() {
        DataService dataService = Mockito.mock(DataService.class);
        Mockito.when(dataService.getDataById(any())).thenReturn("ss");
        Mockito.when(dataService.getDataById(Mockito.eq("1"))).thenReturn("1");
        Mockito.when(dataService.getDataById("2")).thenReturn("2");
        Mockito.when(dataService.getDataById("3")).thenThrow(new RuntimeException());
        Mockito.when(dataService.getDataById(Mockito.argThat(arg ->
                Objects.isNull(arg) || arg.length() > 3))).thenReturn("argThat");

        Assert.assertEquals("1", dataService.getDataById("1"));
        Assert.assertEquals("2", dataService.getDataById("2"));
        Assert.assertEquals("ss", dataService.getDataById("ljk"));
        Assert.assertEquals("ss", dataService.getDataById("mnb"));
        Assert.assertEquals("argThat", dataService.getDataById("mnbmd"));
        Assert.assertEquals("argThat", dataService.getDataById(null));
        Assert.assertNotEquals("argThat", dataService.getDataById("to"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDataById_methodCallSeveralTimes() {
        DataService dataService = Mockito.mock(DataService.class);
        Mockito.when(dataService.getDataById("a"))
                .thenReturn("valueA1", "valueA2")
                .thenThrow(IllegalArgumentException.class);
        String str1 = dataService.getDataById("a");
        String str2 = dataService.getDataById("a");
        String str3 = dataService.getDataById("a");
    }

    @Test(expected = RuntimeException.class)
    public void getDataById_RunTimeException() {
        DataService dataService = Mockito.mock(DataService.class);
        Mockito.when(dataService.getDataById(any())).thenThrow(new RuntimeException("s"));
        dataService.getDataById("2");
    }

    @Test
    public void getDataBiIds() {
        List<String> ids = new ArrayList<>();
        ids.add("q");
        ids.add("a");
        ids.add("b");
        DataService dataService = Mockito.mock(DataService.class);
        Mockito.when(dataService.getDataByIds(any())).thenAnswer(invocation ->
                invocation.<List<String>>getArgument(0).stream().map(id -> {
                    switch (id) {
                        case "a":
                            return "dataItemA";
                        case "b":
                            return "dataItemB";
                        default:
                            return id;
                    }
                }).collect(Collectors.toList()));
        List<String> ar = dataService.getDataByIds(ids);
        List<String> er = new ArrayList<>();
        er.add("q");
        er.add("dataItemA");
        er.add("dataItemB");
        Assert.assertEquals(er, ar);
    }

    @Test
    public void getDataBiIds_MockitoCallRealMethod() {
        List<String> ids = new ArrayList<>();
        ids.add("q");
        ids.add("a");
        ids.add("b");
        MyOverridableClass data = Mockito.mock(MyOverridableClass.class);
        Mockito.when(data.getDataByIds(any())).thenAnswer(InvocationOnMock::callRealMethod);
        List<String> ar = data.getDataByIds(ids);
        List<String> er = Collections.emptyList();
        Assert.assertEquals(er, ar);
    }

}
