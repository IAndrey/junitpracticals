package test_part_8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import part_8.LogAnalyzer;

public class TestLogAnalyzer {
    private LogAnalyzer logAnalyzer;

    @Before
    public void initialize() {
        logAnalyzer = new LogAnalyzer();
        logAnalyzer.initialize();
    }

    @Test()
    public void isValid() {
        Assert.assertTrue(logAnalyzer.isValid("12345"));
    }

    @Test
    public void isValid_false() {
        Assert.assertFalse(logAnalyzer.isValid("12345678"));
    }

    @Test
    public void isValid_false_Message() {
        Assert.assertTrue("my message", logAnalyzer.isValid("12345678"));// если тест не выполняется
        // выведится сообщение
    }
}
