package test_part_4_3;

import org.junit.Assert;
import org.junit.Test;
import part_4.part_4_3.FakeWebService;
import part_4.part_4_3.LogAnalyzer;

public class Test_LogAnalyzer {

    @Test
    public void analyze_TooShortFileName_CallsWebService(){
        FakeWebService mockService = new FakeWebService();
        LogAnalyzer log = new LogAnalyzer(mockService);
        String tooShortFileName = "abc.ext";
        log.analyze(tooShortFileName);

        Assert.assertEquals("Слишком короткое имя файла: abc.ext", mockService.lastError);
    }
}
