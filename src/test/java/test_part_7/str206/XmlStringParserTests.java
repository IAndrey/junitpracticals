package test_part_7.str206;

import org.junit.Assert;
import org.junit.Test;
import part_7.IStringParser;
import part_7.XmlStringParser;

public class XmlStringParserTests extends TemplateStringParserTests {
    protected IStringParser getIStringParser(String input) {
        return new XmlStringParser(input);
    }

    @Test
    @Override
    public void getVersion1() {
        IStringParser parser = getIStringParser("<Header>1</Header>");
        String version = parser.getVersionFromHeader();
        Assert.assertEquals(version, "1");
    }

    @Test
    @Override
    public void getVersion2() {
        IStringParser parser = getIStringParser("<Header>1.1</Header>");
        String version = parser.getVersionFromHeader();
        Assert.assertEquals(version, "1.1");
    }

    @Test
    @Override
    public void getVersion3() {
        IStringParser parser = getIStringParser("<Header>1.1.1</Header>");
        String version = parser.getVersionFromHeader();
        Assert.assertEquals(version, "1.1.1");
    }
}
