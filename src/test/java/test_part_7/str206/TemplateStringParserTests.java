package test_part_7.str206;

import part_7.IStringParser;
import part_7.XmlStringParser;

public abstract class TemplateStringParserTests {

    public abstract void getVersion1();

    public abstract void getVersion2();

    public abstract void getVersion3();
}
