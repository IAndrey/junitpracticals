package test_part_7.str206;

import org.junit.Assert;
import org.junit.Test;
import part_7.StandardStringParser;

public class StandardStringParserTest {
    private StandardStringParser getParser(String input) {
        return new StandardStringParser(input);
    }

    @Test
    public void getVersion(){
        String input = "header;version=1;\n";
        StandardStringParser parser = getParser(input);
        String version = parser.getVersionFromHeader();
        Assert.assertEquals(version, "1");
    }

    @Test
    public void getVersion2(){
        String input = "header;version=1.1;\n";
        StandardStringParser parser = getParser(input);
        String version = parser.getVersionFromHeader();
        Assert.assertEquals(version, "1.1");
    }

    @Test
    public void getVersion3(){
        String input = "header;version=1.1.1;\n";
        StandardStringParser parser = getParser(input);
        String version = parser.getVersionFromHeader();
        Assert.assertEquals(version, "1.1.1");
    }
}
