package test_part_7.str207;

import part_7.IStringParser;
import part_7.StandardStringParser;

public class StandardStringParserTest extends TemplateStringParserTests {

    public StandardStringParserTest() {
    }

    @Override
    protected IStringParser getParser(String input) {
        return new StandardStringParser(input);
    }

    @Override
    protected String HeaderVersion_SingleDigit() {
        return String.format("header\tversion=%s\t\n", EXPECTED_SINGLE_DIGIT);
    }

    protected String HeaderVersion_MinorVersion() {
        return String.format("header\tversion=%s\t\n", EXPECTED_WITH_MINORVERSION);
    }

    protected String HeaderVersion_WithRevision() {
        return String.format("header\tversion=%s\t\n", EXPECTED_WITH_REVISION);
    }
}
