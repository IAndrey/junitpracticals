package test_part_7.str207;

import org.junit.Assert;
import org.junit.Test;
import part_7.IStringParser;

public abstract class TemplateStringParserTests {

    public TemplateStringParserTests() {
    }

    protected abstract IStringParser getParser(String input);

    protected abstract String HeaderVersion_SingleDigit();

    protected abstract String HeaderVersion_MinorVersion();

    protected abstract String HeaderVersion_WithRevision();

    public final String EXPECTED_SINGLE_DIGIT = "1";
    public final String EXPECTED_WITH_MINORVERSION = "1.1";
    public final String EXPECTED_WITH_REVISION = "1.1.1";

    @Test
    public void getStringVersionFromHeader_SingleDigit_Found(){
        String input = HeaderVersion_SingleDigit();
        IStringParser parser = getParser(input);
        String versionHeader = parser.getVersionFromHeader();
        Assert.assertEquals(EXPECTED_SINGLE_DIGIT, versionHeader);
    }

    @Test
    public void getStringVersionFromHeader_WithMinor_Found(){
        String input = HeaderVersion_MinorVersion();
        IStringParser parser = getParser(input);
        String versionHeader = parser.getVersionFromHeader();
        Assert.assertEquals(EXPECTED_WITH_MINORVERSION, versionHeader);
    }

    @Test
    public void getStringVersionFromHeader_WithRevision_Found(){
        String input = HeaderVersion_WithRevision();
        IStringParser parser = getParser(input);
        String versionHeader = parser.getVersionFromHeader();
        Assert.assertEquals(EXPECTED_WITH_REVISION, versionHeader);
    }
}
