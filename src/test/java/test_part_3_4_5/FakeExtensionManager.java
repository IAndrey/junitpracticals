package test_part_3_4_5;

import Origin.IExtensionManager;

public class FakeExtensionManager implements IExtensionManager {
    public  boolean willBeValid = false;
    @Override
    public boolean isValid(String fileName) {
        return willBeValid;
    }
}
