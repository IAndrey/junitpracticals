package test_part_3_4_5;

import org.junit.Assert;
import org.junit.Test;
import part_3_4_5.LogAnalyzer;

public class LogAnalizerTest {

    @Test
    public void isValidFileName_BadExtension_ReturnsTrue(){
        FakeExtensionManager fakeExtensionManager = new FakeExtensionManager();
        fakeExtensionManager.willBeValid = true;

        LogAnalyzer logAnalyzer = new LogAnalyzer();
        logAnalyzer.setManager(fakeExtensionManager);
        Assert.assertTrue(logAnalyzer.isValidLogFileName(""));
    }
}
