package test_part_5.t5_3;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import part_5.c5_3.ILogger;
import part_5.c5_3.LogAnalyzer;

public class LogAnalyzerTests {

    @Test
    public void analyze_toShortFileName() {
        ILogger logger = Mockito.mock(ILogger.class);
        LogAnalyzer analyzer = new LogAnalyzer(logger);
        analyzer.minNameLength = 6;
        analyzer.analyze("a.txt");
        //Проверяю, что метод logError в  logger был вызван 1 раз с параметром Слишком короткое имя файла: a.txt
        Mockito.verify(logger, Mockito.times(1)).logError("Слишком короткое имя файла: a.txt");

        //Указываю объекту logger какое буаленовское значение возвращать при вызове метода logger.logError с указанными
//        параметрами
        Mockito.when(logger.logError("a.text")).thenReturn(false);
        Mockito.when(logger.logError("asd.text")).thenReturn(true);
        Assert.assertFalse(logger.logError("a.text"));
        Assert.assertTrue(logger.logError("asd.text"));
    }
}
