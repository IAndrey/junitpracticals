package test_part_5.t5_2;

import org.junit.Test;
import org.mockito.Mockito;
import part_5.c5_2.ILogger;
import part_5.c5_2.LogAnalyzer;

public class LogAnalyzerTests {

    @Test
    public void analyze_toShortFileName() {
        ILogger logger = Mockito.mock(ILogger.class);
        LogAnalyzer analyzer = new LogAnalyzer(logger);
        analyzer.minNameLength = 6;
        analyzer.analyze("a.txt");
        //Проверяю, что метод logError в  logger был вызван 1 раз с параметром Слишком короткое имя файла: a.txt
        Mockito.verify(logger, Mockito.times(1)).logError("Слишком короткое имя файла: a.txt");
    }
}
