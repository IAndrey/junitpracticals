package test_part_5.t5_4;

import org.junit.Test;
import org.mockito.Mockito;
import part_5.c5_4.IView;
import part_5.c5_4.Presenter;

public class PresenterTest {

    @Test
    public void thenViewIsLoadedRenderMethod() {
        IView view = Mockito.mock(IView.class);
        Presenter p = new Presenter(view);
        //Проверяем что был вызван приватный метод render в конструкторе класса Presenter, 1 раз с параметром
//        Hello word;
        Mockito.verify(view, Mockito.times(1)).render("Hello word");
    }
}
