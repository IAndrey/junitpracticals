package test_part_4_4;


import org.junit.Assert;
import org.junit.Test;
import part_4.c4_4.FakeEmailService;
import part_4.c4_4.FakeWebService;
import part_4.c4_4.LogAnalyzer2;

public class Test_LogAnalyzer2 {

    @Test
    public void analyze_webServiceThrows_SendsEmail() {
        FakeWebService stubService = new FakeWebService();
        stubService.toThrow = new Exception("fake exception");

        FakeEmailService mockEmail = new FakeEmailService();
        LogAnalyzer2 log = new LogAnalyzer2(stubService, mockEmail);
        String tooShortFileName = "abc.ext";
        log.analyze(tooShortFileName);
        Assert.assertEquals("someone@somewhere.com", mockEmail.to);
        Assert.assertEquals("fake exception", mockEmail.body);
        Assert.assertEquals("can`t log", mockEmail.subject);
    }
}
