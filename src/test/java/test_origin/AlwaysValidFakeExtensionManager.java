package test_origin;

import Origin.IExtensionManager;

public class AlwaysValidFakeExtensionManager implements IExtensionManager {
    public boolean isValid(String fileName) {
        return true;
    }
}
