package test_origin;

import Origin.LogAnalyzer;
import org.junit.Assert;
import org.junit.Test;

public class LogAnalizerTest {
    public interface FastTests {
    }

    @Test
    public void isValidFileName_BadExtension_ReturnsFalse(){
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        Assert.assertFalse(logAnalyzer.isValidLogFileName("asdsadsad.asdadd"));
    }

    @Test
    public void isValidFileName_BadExtension_ReturnsTrue(){
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        Assert.assertTrue(logAnalyzer.isValidLogFileName("asdsadsad.slf"));
    }

    @Test
    public void isValidFileName_BadExtension_ReturnsTrueUpperCase(){
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        Assert.assertTrue(logAnalyzer.isValidLogFileName("asdsadsad.SLF"));
    }

    @Test
//    @Category(FastTests.class)
    public void isValidFileName_BadExtension_ReturnsTrueUpperCase_cat(){
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        Assert.assertTrue(logAnalyzer.isValidLogFileName("asdsadsad.SLF"));
    }
}
