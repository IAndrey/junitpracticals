package test_origin;

import Origin.LogAnalyzer;
import org.junit.Test;

public class LogAnalizer_2_6_2_test {

    @Test(expected = IllegalArgumentException.class)
    public void isValidFileName_BadExtension_ReturnsTrue(){
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        logAnalyzer.isValidLogFileName("");
    }
}
