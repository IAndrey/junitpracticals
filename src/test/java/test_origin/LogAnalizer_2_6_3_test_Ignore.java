package test_origin;

import Origin.LogAnalyzer;
import org.junit.Ignore;
import org.junit.Test;

public class LogAnalizer_2_6_3_test_Ignore {
    @Test(expected = IllegalArgumentException.class)
    public void isValidFileName_BadExtension_ReturnsTrue(){
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        logAnalyzer.isValidLogFileName("");
    }

    @Ignore
    @Test
    public void isValidFileName(){
        LogAnalyzer logAnalyzer = new LogAnalyzer();
    }
}
