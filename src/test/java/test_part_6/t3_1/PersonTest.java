package test_part_6.t3_1;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import part_6.c3_1.IPerson;

public class PersonTest {

    @Test
    public void iPerson(){
        IPerson p = Mockito.mock(IPerson.class);
        Mockito.when(p.getManager()).thenReturn(Mockito.mock(IPerson.class));
        Assert.assertNotNull(p.getManager());
        Assert.assertNotNull(p.getManager().getManager());
        Assert.assertNotNull(p.getManager().getManager().getManager());
    }
}
