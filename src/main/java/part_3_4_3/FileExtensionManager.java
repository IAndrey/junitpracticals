package part_3_4_3;

import Origin.IExtensionManager;

public class FileExtensionManager implements IExtensionManager {

    @Override
    public boolean isValid(String fileName) {
        if (fileName.isEmpty()) throw new IllegalArgumentException("Имя должно быть задано");

        return fileName.toLowerCase().endsWith(".slf");
    }
}
