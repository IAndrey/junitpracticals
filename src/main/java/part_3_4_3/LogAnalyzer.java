package part_3_4_3;

import Origin.IExtensionManager;

public class LogAnalyzer {
    private IExtensionManager manager;

    public LogAnalyzer(IExtensionManager mgr) {
        manager = mgr;
    }

    public boolean isValidLogFileName(String fileName) {
        return manager.isValid(fileName);
    }
}
