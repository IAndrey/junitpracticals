package part_3_4_3;

public interface IExtensionManager {
    boolean isValid(String fileName);
}
