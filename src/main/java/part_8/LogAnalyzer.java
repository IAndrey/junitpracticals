package part_8;

public class LogAnalyzer {
    private boolean initialize;

    public boolean isValid(String name) {
        if (!initialize) {
            throw new RuntimeException();
        }
        return name.length() < 8;
    }

    public void initialize() {
        initialize = true;
    }

}
