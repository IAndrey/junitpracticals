import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Rim {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String str = reader.readLine();
            System.out.println(formatToRim(str));
        }
    }

    private static String formatToRim(String str) {
        int num = Integer.parseInt(str);
        int countX = num / 10;
        int countV = num % 10 / 5;
        int countI = num % 10 % 5;
        return rim(countX, countV, countI);
    }

    private static String rim(int countX, int countV, int countI) {
        StringBuilder result = new StringBuilder();
        while (countX > 0) {
            result.append("X");
            countX--;
        }

        while (countV > 0) {
            result.append("V");
            countV--;
        }

        if (countI == 4) {
            result.append("IV");
        } else {
            while (countI > 0) {
                result.append("I");
                countI--;
            }
        }
        return result.toString().replaceAll("VIV", "IX");
    }
}
