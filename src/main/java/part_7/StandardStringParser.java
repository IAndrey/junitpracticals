package part_7;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StandardStringParser implements IStringParser{
    String input;

    public StandardStringParser(String input) {
        this.input = input;
    }

    public String getVersionFromHeader() {
        Pattern pattern = Pattern.compile("=.+[;\t\n]");
        Matcher match = pattern.matcher(input);
        return match.find()? match.group().replaceAll("[=;\t\n]", ""): null;
    }
}
