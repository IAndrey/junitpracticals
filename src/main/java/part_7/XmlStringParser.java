package part_7;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlStringParser implements IStringParser {

    String input;

    public XmlStringParser(String input) {
        this.input = input;
    }

    public String getVersionFromHeader() {
        Pattern pattern = Pattern.compile(">.+<");
        Matcher match = pattern.matcher(input);
        return match.find() ? match.group().replaceAll("[><]", "") : null;
    }
}
