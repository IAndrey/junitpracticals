package part_3_4_5;

import Origin.IExtensionManager;

public class LogAnalyzer {
    private IExtensionManager manager;

    public IExtensionManager getManager() {
        return manager;
    }

    public void setManager(IExtensionManager manager) {
        this.manager = manager;
    }

    public LogAnalyzer() {
        manager = new FileExtensionManager();
    }

    public boolean isValidLogFileName(String fileName) {
        return manager.isValid(fileName);
    }
}
