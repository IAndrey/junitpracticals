package mockito;

import java.util.List;
import java.util.function.Supplier;

public interface DataService {
    void saveData(List<String> dataToSave);

    String getDataById(String id);

    String getDataById(String id, Supplier<String> calculateIfAbsent);

    List<String> getData();

    List<String> getDataByIds(List<String> idList);

    List<String> getDataByRequest(DataSearchRequest request);
}
