package mockito;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class DataSearchRequest {

    private String id;
    private Date updatedBefore;
    private int length;

}
