package part_3_5_1;

import Origin.IExtensionManager;
import part_3_4_6.ExtensionManagerFactory;

public class LogAnalyzer {
    private IExtensionManager manager;

    public LogAnalyzer() {
        manager = ExtensionManagerFactory.create();
    }

    public boolean isValidLogFileName(String fileName) {
        return manager.isValid(fileName);
    }
}
