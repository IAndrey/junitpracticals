package part_5.c5_2;

public interface ILogger {
    void logError(String message);
}
