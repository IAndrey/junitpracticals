package part_5.c5_2;


public class LogAnalyzer {
    private ILogger service;
    public int minNameLength;

    public LogAnalyzer(ILogger service) {
        this.service = service;
    }

    public void analyze(String fileName) {
        if (fileName.length() < minNameLength) {
            service.logError("Слишком короткое имя файла: " + fileName);
        }
    }

    public ILogger getService() {
        return service;
    }

    public void setMinNameLength(int minNameLength) {
        this.minNameLength = minNameLength;
    }
}
