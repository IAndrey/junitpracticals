package part_5.c5_3;

public interface ILogger {
    boolean logError(String message);
}
