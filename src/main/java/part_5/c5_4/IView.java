package part_5.c5_4;

public interface IView {
    void render(String text);
}
