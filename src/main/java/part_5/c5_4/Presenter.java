package part_5.c5_4;

public class Presenter {
    private IView iView;

    public Presenter(IView iView) {
        this.iView = iView;
        onLoaded();
    }

    private void onLoaded(){
        iView.render("Hello word");
    }
}
