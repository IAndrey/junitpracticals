package part_3_4_6;

import Origin.IExtensionManager;

public class LogAnalyzer {
    private IExtensionManager manager;

    public LogAnalyzer() {
        manager = ExtensionManagerFactory.create();
    }

    public boolean isValidLogFileName(String fileName) {
        return manager.isValid(fileName);
    }
}
