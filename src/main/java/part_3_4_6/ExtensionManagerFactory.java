package part_3_4_6;

import Origin.IExtensionManager;

public class ExtensionManagerFactory {
    static IExtensionManager customManager = null;

    static public IExtensionManager create() {
        if (customManager != null) {
            return customManager;
        }
        return new FileExtensionManager();
    }

    static public void setManager(IExtensionManager mgr) {
        customManager = mgr;
    }
}
