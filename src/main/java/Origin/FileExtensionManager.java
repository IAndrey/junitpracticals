package Origin;

public class FileExtensionManager implements IExtensionManager {

    @Override
    public boolean isValid(String fileName) {
        if (fileName.isEmpty()) throw new IllegalArgumentException("Имя должно быть задано");

        return fileName.toLowerCase().endsWith(".slf");
    }
}
