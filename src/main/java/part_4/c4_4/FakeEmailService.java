package part_4.c4_4;

public class FakeEmailService implements IEmailService {
    public String to;
    public String subject;
    public String body;

    public void sendEmail(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }
}
