package part_4.c4_4;


public class FakeWebService implements IWebService {
    public Exception toThrow;

    public void logError(String message) throws Exception {
        if (toThrow != null) {
            throw toThrow;
        }
    }
}
