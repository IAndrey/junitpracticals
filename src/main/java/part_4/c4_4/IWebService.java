package part_4.c4_4;

public interface IWebService {
    void logError(String message) throws Exception;
}
