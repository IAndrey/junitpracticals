package part_4.c4_4;

public interface IEmailService {

    void sendEmail(String to, String subject, String body);

}
