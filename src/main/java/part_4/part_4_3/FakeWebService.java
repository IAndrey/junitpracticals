package part_4.part_4_3;

public class FakeWebService implements IWebService {
    public String lastError;

    @Override
    public void logError(String message) {
        lastError = message;
    }
}
