package part_4.part_4_3;

public class LogAnalyzer {
    private IWebService service;

    public LogAnalyzer(IWebService service) {
        this.service = service;
    }

    public void analyze(String fileName) {
        if (fileName.length() < 8) {
            service.logError("Слишком короткое имя файла: " + fileName);
        }
    }
}
